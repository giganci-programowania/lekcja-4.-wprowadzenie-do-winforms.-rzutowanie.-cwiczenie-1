﻿namespace lekcja_4._winforms_i_rzutowanie_cw_1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMatematyka = new System.Windows.Forms.Label();
            this.lblPolski = new System.Windows.Forms.Label();
            this.lblFizyka = new System.Windows.Forms.Label();
            this.lblAngielski = new System.Windows.Forms.Label();
            this.lblInformatyka = new System.Windows.Forms.Label();
            this.txtMatematyka = new System.Windows.Forms.TextBox();
            this.txtPolski = new System.Windows.Forms.TextBox();
            this.txtFizyka = new System.Windows.Forms.TextBox();
            this.txtAngielski = new System.Windows.Forms.TextBox();
            this.txtInformatyka = new System.Windows.Forms.TextBox();
            this.lblWynik = new System.Windows.Forms.Label();
            this.txtWynik = new System.Windows.Forms.TextBox();
            this.btnOblicz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblMatematyka
            // 
            this.lblMatematyka.AutoSize = true;
            this.lblMatematyka.Location = new System.Drawing.Point(56, 15);
            this.lblMatematyka.Name = "lblMatematyka";
            this.lblMatematyka.Size = new System.Drawing.Size(103, 13);
            this.lblMatematyka.TabIndex = 0;
            this.lblMatematyka.Text = "Ocena z matematyki";
            // 
            // lblPolski
            // 
            this.lblPolski.AutoSize = true;
            this.lblPolski.Location = new System.Drawing.Point(64, 41);
            this.lblPolski.Name = "lblPolski";
            this.lblPolski.Size = new System.Drawing.Size(95, 13);
            this.lblPolski.TabIndex = 1;
            this.lblPolski.Text = "Ocena z polskiego";
            // 
            // lblFizyka
            // 
            this.lblFizyka.AutoSize = true;
            this.lblFizyka.Location = new System.Drawing.Point(86, 69);
            this.lblFizyka.Name = "lblFizyka";
            this.lblFizyka.Size = new System.Drawing.Size(73, 13);
            this.lblFizyka.TabIndex = 2;
            this.lblFizyka.Text = "Ocena z fizyki";
            // 
            // lblAngielski
            // 
            this.lblAngielski.AutoSize = true;
            this.lblAngielski.Location = new System.Drawing.Point(50, 95);
            this.lblAngielski.Name = "lblAngielski";
            this.lblAngielski.Size = new System.Drawing.Size(109, 13);
            this.lblAngielski.TabIndex = 3;
            this.lblAngielski.Text = "Ocena z angielskiego";
            // 
            // lblInformatyka
            // 
            this.lblInformatyka.AutoSize = true;
            this.lblInformatyka.Location = new System.Drawing.Point(59, 121);
            this.lblInformatyka.Name = "lblInformatyka";
            this.lblInformatyka.Size = new System.Drawing.Size(100, 13);
            this.lblInformatyka.TabIndex = 4;
            this.lblInformatyka.Text = "Ocena z informatyki";
            // 
            // txtMatematyka
            // 
            this.txtMatematyka.Location = new System.Drawing.Point(175, 12);
            this.txtMatematyka.Name = "txtMatematyka";
            this.txtMatematyka.Size = new System.Drawing.Size(100, 20);
            this.txtMatematyka.TabIndex = 5;
            // 
            // txtPolski
            // 
            this.txtPolski.Location = new System.Drawing.Point(175, 38);
            this.txtPolski.Name = "txtPolski";
            this.txtPolski.Size = new System.Drawing.Size(100, 20);
            this.txtPolski.TabIndex = 6;
            // 
            // txtFizyka
            // 
            this.txtFizyka.Location = new System.Drawing.Point(175, 66);
            this.txtFizyka.Name = "txtFizyka";
            this.txtFizyka.Size = new System.Drawing.Size(100, 20);
            this.txtFizyka.TabIndex = 7;
            // 
            // txtAngielski
            // 
            this.txtAngielski.Location = new System.Drawing.Point(175, 92);
            this.txtAngielski.Name = "txtAngielski";
            this.txtAngielski.Size = new System.Drawing.Size(100, 20);
            this.txtAngielski.TabIndex = 8;
            // 
            // txtInformatyka
            // 
            this.txtInformatyka.Location = new System.Drawing.Point(175, 118);
            this.txtInformatyka.Name = "txtInformatyka";
            this.txtInformatyka.Size = new System.Drawing.Size(100, 20);
            this.txtInformatyka.TabIndex = 9;
            // 
            // lblWynik
            // 
            this.lblWynik.AutoSize = true;
            this.lblWynik.Location = new System.Drawing.Point(50, 182);
            this.lblWynik.Name = "lblWynik";
            this.lblWynik.Size = new System.Drawing.Size(37, 13);
            this.lblWynik.TabIndex = 10;
            this.lblWynik.Text = "Wynik";
            // 
            // txtWynik
            // 
            this.txtWynik.Location = new System.Drawing.Point(93, 179);
            this.txtWynik.Name = "txtWynik";
            this.txtWynik.Size = new System.Drawing.Size(100, 20);
            this.txtWynik.TabIndex = 11;
            // 
            // btnOblicz
            // 
            this.btnOblicz.Location = new System.Drawing.Point(200, 177);
            this.btnOblicz.Name = "btnOblicz";
            this.btnOblicz.Size = new System.Drawing.Size(75, 23);
            this.btnOblicz.TabIndex = 12;
            this.btnOblicz.Text = "Oblicz";
            this.btnOblicz.UseVisualStyleBackColor = true;
            this.btnOblicz.Click += new System.EventHandler(this.btnOblicz_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(349, 249);
            this.Controls.Add(this.btnOblicz);
            this.Controls.Add(this.txtWynik);
            this.Controls.Add(this.lblWynik);
            this.Controls.Add(this.txtInformatyka);
            this.Controls.Add(this.txtAngielski);
            this.Controls.Add(this.txtFizyka);
            this.Controls.Add(this.txtPolski);
            this.Controls.Add(this.txtMatematyka);
            this.Controls.Add(this.lblInformatyka);
            this.Controls.Add(this.lblAngielski);
            this.Controls.Add(this.lblFizyka);
            this.Controls.Add(this.lblPolski);
            this.Controls.Add(this.lblMatematyka);
            this.Name = "Form1";
            this.Text = "Kalkulator średniej";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMatematyka;
        private System.Windows.Forms.Label lblPolski;
        private System.Windows.Forms.Label lblFizyka;
        private System.Windows.Forms.Label lblAngielski;
        private System.Windows.Forms.Label lblInformatyka;
        private System.Windows.Forms.TextBox txtMatematyka;
        private System.Windows.Forms.TextBox txtPolski;
        private System.Windows.Forms.TextBox txtFizyka;
        private System.Windows.Forms.TextBox txtAngielski;
        private System.Windows.Forms.TextBox txtInformatyka;
        private System.Windows.Forms.Label lblWynik;
        private System.Windows.Forms.TextBox txtWynik;
        private System.Windows.Forms.Button btnOblicz;
    }
}

