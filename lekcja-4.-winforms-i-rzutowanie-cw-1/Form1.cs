﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lekcja_4._winforms_i_rzutowanie_cw_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnOblicz_Click(object sender, EventArgs e)
        {
            float ocenaMatematyka = float.Parse(txtMatematyka.Text);
            float ocenaPolski = float.Parse(txtPolski.Text);
            float ocenaFizyka = float.Parse(txtFizyka.Text);
            float ocenaAngielski = float.Parse(txtAngielski.Text);
            float ocenaInormatyka = float.Parse(txtInformatyka.Text);

            float sumaOcen = ocenaMatematyka + ocenaFizyka + ocenaAngielski + ocenaPolski + ocenaInormatyka;
            float srednia = sumaOcen / 5;

            txtWynik.Text = srednia.ToString();
        }
    }
}
